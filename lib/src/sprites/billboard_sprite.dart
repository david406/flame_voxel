part of flame_voxel;

class BillboardSprite extends Sprite with BatchRenderable {
  final double index = 4;

  final Vector2 position;

  final Rect rect;

  final Vector3 dimensions;

  Vector2 offset;

  BillboardSprite({
    @required Vector2 offset,
    @required this.position,
    @required Vector2 size,
  })  : dimensions = Vector3(size.x, size.y, 1),
        rect = Rect.fromLTWH(offset.x, offset.y, size.x, size.y);

  @override
  void batchRender(SpriteBatch batch) {
    final anchor = Offset(dimensions.x / 2, dimensions.y / 2);
    batch.add(
      rect: rect,
      offset: Offset(offset.x, offset.y) + (anchor * world.camera.zoom),
      scale: world.camera.zoom,
      rotation: -world.camera.rotation,
      anchor: anchor,
    );
  }
}

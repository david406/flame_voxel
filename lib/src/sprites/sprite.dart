part of flame_voxel;

/// The base for all objects to be rendered on the screen.
class Sprite {
  double index;

  SpriteWorld world;

  bool visible = true;
}

mixin Renderable on Sprite {
  void render(Canvas canvas);
}

mixin BatchRenderable on Sprite {
  void batchRender(SpriteBatch batch);
}

mixin Updateable on Sprite {
  void update(double delta);
}

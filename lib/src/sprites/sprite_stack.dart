part of flame_voxel;

class SpriteStack extends Sprite {
  final _layers = <LayerSprite>[];

  final int type;

  Vector2 position;

  final Vector3 dimensions;

  SpriteStack({
    this.type,
    this.dimensions,
    this.position,
  })  : assert(type != null),
        assert(dimensions != null &&
            dimensions.x > 0 &&
            dimensions.y > 0 &&
            dimensions.z > 0),
        assert(position != null);

  void _createLayers(double scale) {
    var qualityValue = scale / 100 * world.quality.value;
    qualityValue = qualityValue == 0 || dimensions.z == 1 ? 1 : qualityValue;
    _layers.clear();
    
    for (var i = 0; i < dimensions.z; i++) {
      /// This second loop ensures that there are extra layers between the current layer and the next one(or previous if last).
      /// This allows for a more solid look with no rigid edges. So if the scale value is 4, we add 4 extra layers between the current and the next one.
      /// This comes at a cost, for each layer we add, we add `scale * height` layers extra.
      for (var j = 0; j < qualityValue; j++) {
        _layers.add(
          LayerSprite(
            i + (j / scale),
            Rect.fromLTWH(i * dimensions.x, type * dimensions.y, dimensions.x,
                dimensions.y),
            Vector2.zero(),
            this,
          ),
        );
      }
    }
  }
}

import 'dart:ui';

import 'package:flame/sprite_batch.dart';
import 'package:flame_voxel/flame_voxel.dart';
import 'package:vector_math/vector_math_64.dart';

class LayerSprite extends Sprite with BatchRenderable {
  final double index;

  final Rect rect;

  Vector2 get position => parent.position;

  Vector2 offset;

  final SpriteStack parent;

  LayerSprite(
    this.index,
    this.rect,
    this.offset,
    this.parent,
  );

  @override
  void batchRender(SpriteBatch batch) {
    batch.add(
      rect: rect,
      offset: Offset(offset.x, offset.y),
      scale: world.camera.zoom,
    );
  }
}

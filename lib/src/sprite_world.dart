part of flame_voxel;

class SpriteWorld {
  /// Only used as the origin for all the [_secondaryBatches].
  final SpriteBatch _mainBatch;

  /// Dynamically assigned batches for rendering, copies of [_mainBatch].
  final List<SpriteBatch> _secondaryBatches = [];

  final List<SpriteStack> spriteStacks;

  final List<BillboardSprite> billboardSprites;

  final Camera2D camera;

  /// Control the quality of the [spriteStacks].
  final SpriteStackQuality quality;

  List<Sprite> get _sprites {
    return spriteStacks.fold<List<Sprite>>(
      [],
      (sprites, stack) => sprites..addAll(stack._layers),
    )
      ..addAll(billboardSprites)
      ..sort((a, b) => a.index == b.index ? 0 : a.index < b.index ? -1 : 1);
  }

  SpriteWorld(
    this._mainBatch,
    this.spriteStacks,
    this.camera, {
    this.billboardSprites,
    this.quality = SpriteStackQuality.HIGH,
  })  : assert(_mainBatch != null),
        assert(spriteStacks != null && spriteStacks.isNotEmpty) {
    camera.onZoomChanged.listen(_onZoomChanged);
    camera.onRotationChanged.listen(_rotateSpriteStacks);

    spriteStacks.forEach((stack) => stack._createLayers(camera.zoom));
    _rotateSpriteStacks(camera.rotation);
  }

  /// Create a new VoxelWorld with given assets.
  static Future<SpriteWorld> withAsset(
    String imageName, {
    @required List<SpriteStack> spriteStacks,
    List<BillboardSprite> billboardSprites,
    Rect viewport,
    Camera2D camera,
    SpriteStackQuality quality,
  }) async {
    assert(
      viewport == null && camera != null || viewport != null && camera == null,
    );

    return SpriteWorld(
      await SpriteBatch.withAsset(imageName),
      spriteStacks,
      camera ?? Camera2D(viewport),
      billboardSprites: billboardSprites ?? [],
      quality: quality ?? SpriteStackQuality.HIGH,
    );
  }

  void add(Sprite sprite, {bool update = true}) {
    if (sprite is SpriteStack) {
      spriteStacks.add(sprite);
    } else if (sprite is BillboardSprite) {
      billboardSprites.add(sprite);
    }

    if (update) {
      spriteStacks.forEach((stack) => stack._createLayers(camera.zoom));
      _rotateSpriteStacks(camera.rotation);
    }
  }

  @mustCallSuper
  void dispose() => camera.dispose();

  @mustCallSuper
  void render(Canvas canvas) {
    // Clear all existing batches.
    _secondaryBatches.forEach((batch) => batch.clear());

    var batchCounter = 0;
    _sprites.forEach((renderable) {
      renderable.world = this;
      if (!renderable.visible) {
        return;
      }

      if (renderable is BatchRenderable) {
        if (_secondaryBatches.length <= batchCounter) {
          _secondaryBatches.add(SpriteBatch(_mainBatch.atlas));
        }
        renderable.batchRender(_secondaryBatches[batchCounter]);
      } else if (renderable is Renderable) {
        batchCounter++;
        renderable.render(canvas);
      }
    });
    _secondaryBatches.removeRange(batchCounter + 1, _secondaryBatches.length);

    canvas
      ..save()
      ..clipRect(camera.bounds)
      ..transform(camera.transformMatrix.storage)
      ..translate(camera.position.x, camera.position.y)
      ..rotate(camera.rotation)
      ..translate(-camera.position.x, camera.position.y);

    _secondaryBatches.forEach((batch) => batch.render(canvas));

    canvas.restore();
  }

  @mustCallSuper
  void update(double delta) {
    spriteStacks.forEach((stack) {
      if (stack is Updateable) {
        (stack as Updateable).update(delta);
      }
    });
    billboardSprites.forEach((sprite) {
      if (sprite is Updateable) {
        (sprite as Updateable).update(delta);
      }
    });
  }

  void _onZoomChanged(double zoom) {
    spriteStacks.forEach((stack) => stack._createLayers(zoom));
    _rotateSpriteStacks(camera.rotation);
  }

  void _rotateSpriteStacks(double rotation) {
    _mainBatch.clear();

    _sprites.forEach((renderable) {
      // TODO support z-axis? If so we need to have a way to set a maxium stack height to use as the z-step.
      var z = 0;
      var angle = 0;

      if (renderable is LayerSprite) {
        final stack = renderable.parent;
        final angleX = -sin(angle) * (renderable.index + stack.dimensions.z);

        renderable.offset = Vector2(
          -sin(rotation) *
                  ((renderable.index + (z * stack.dimensions.z)) *
                      camera.zoom) +
              ((renderable.position.x * stack.dimensions.x) * camera.zoom) +
              angleX,
          -cos(rotation) *
                  ((renderable.index + (z * stack.dimensions.z)) *
                      camera.zoom) +
              ((renderable.position.y * stack.dimensions.y) * camera.zoom) +
              angleX,
        );

        // Cull the renderable so we dont draw it when it is outside the camera bounds.
        // TODO: Currently does not work properly because the matrix is not aware of both the zoom and rotation.
        // var layerPos = camera.screenToWorld(renderable.offset);
        // if (layerPos.x > camera.bounds.width ||
        //     layerPos.x < camera.bounds.left ||
        //     layerPos.y > camera.bounds.height ||
        //     layerPos.y < camera.bounds.top) {
        //   renderable.visible = false;
        // } else {
        //   renderable.visible = true;
        // }
      } else if (renderable is BillboardSprite) {
        final angleX =
            -sin(angle) * (renderable.index + renderable.dimensions.z);

        renderable.offset = Vector2(
          -sin(rotation) +
              ((renderable.position.x * renderable.dimensions.x) *
                  camera.zoom) +
              angleX,
          -cos(rotation) +
              ((renderable.position.y * renderable.dimensions.y) *
                  camera.zoom) +
              angleX,
        );
      }
    });
  }
}

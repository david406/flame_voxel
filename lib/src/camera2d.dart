part of flame_voxel;

class Camera2D {
  double _zoom;
  final _onZoomChangedController = StreamController<double>();

  double _rotation;
  final _onRotationChangedController = StreamController<double>();

  Vector2 _position;
  final _onPositionChangedController = StreamController<Vector2>();

  Rect _bounds;
  final _onBoundsChangedController = StreamController<Rect>();

  /// Minimal zoom level possible.
  double minZoom;

  /// Maximal zoom level possible.
  double maxZoom;

  /// Zoom level of the camera.
  double get zoom => _zoom;
  set zoom(double value) {
    if (value >= minZoom && value <= maxZoom) {
      _onZoomChangedController.add(_zoom = value);
    }
  }

  /// Rotation of the camera.
  double get rotation => _rotation;
  set rotation(double value) =>
      _onRotationChangedController.add(_rotation = value);

  /// Position of the camera.
  Vector2 get position => _position;
  set position(Vector2 value) =>
      _onPositionChangedController.add(_position = value);

  /// The boundaries of the camera.
  Rect get bounds => _bounds;
  set bounds(Rect value) => _onBoundsChangedController.add(_bounds = value);

  Stream<double> onZoomChanged;

  Stream<double> onRotationChanged;

  Stream<Vector2> onPositionChanged;

  Stream<Rect> onBoundsChanged;

  /// The center of the camera.
  Vector2 get center => Vector2(bounds.center.dx, bounds.center.dy);

  /// Transform matrix that can be used for [Canvas.transform].
  Matrix4 get transformMatrix =>
      Matrix4.translation(Vector3(-position.x, -position.y, 0)) *
      Matrix4.translation(Vector3(bounds.width * 0.5, bounds.height * 0.5, 0));

  Camera2D(
    this._bounds, {
    double zoom = 1.0,
    this.minZoom = 1.0,
    this.maxZoom = 8.0,
    double rotation: 0,
    Vector2 position,
  }) : assert(zoom > 0) {
    _zoom = zoom;
    _rotation = rotation;
    _position = position ?? Vector2.zero();

    onZoomChanged = _onZoomChangedController.stream;
    onRotationChanged = _onRotationChangedController.stream;
    onPositionChanged = _onPositionChangedController.stream;
    onBoundsChanged = _onBoundsChangedController.stream;
  }

  @mustCallSuper
  void dispose() {
    _onZoomChangedController.close();
    _onRotationChangedController.close();
    _onPositionChangedController.close();
    _onBoundsChangedController.close();
  }

  /// Convert screen position to world position.
  Vector2 screenToWorld(Vector2 position) {
    final matrix = transformMatrix;

    return Vector2(
      (position.x * matrix.storage[0]) +
          (position.x * matrix.storage[1]) +
          matrix.storage[12],
      (position.y * matrix.storage[1]) +
          (position.y * matrix.storage[2]) +
          matrix.storage[13],
    );
  }

  /// Convert world position to screen position.
  Vector2 worldToScreen(Vector2 position) {
    final matrix = Matrix4.inverted(transformMatrix);

    return Vector2(
      (position.x * matrix.storage[0]) +
          (position.x * matrix.storage[1]) +
          matrix.storage[12],
      (position.y * matrix.storage[1]) +
          (position.y * matrix.storage[2]) +
          matrix.storage[13],
    );
  }
}

part of flame_voxel;

/// Controls the quality of the [SpriteStack]s drawn to the screen.
///
/// The quality is determined using the [SpriteStack.value] property. 
/// This value should be between `0` and `100` and is used as a percentage.
class SpriteStackQuality {
  static const NONE = SpriteStackQuality(0);

  static const MEDIUM = SpriteStackQuality(50);

  static const HIGH = SpriteStackQuality(100);

  /// Percentage value of the quality.
  final double value;

  const SpriteStackQuality(this.value) : assert(value >= 0 && value <= 100);
}

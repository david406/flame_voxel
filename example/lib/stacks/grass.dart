part of voxels;

class Grass extends SpriteStack {
  Grass(Vector2 position)
      : super(
          type: 0,
          dimensions: Vector3(8, 8, 1),
          position: position,
        );
}

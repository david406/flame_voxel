library voxels;

import 'package:flame_voxel/flame_voxel.dart';

part 'floor.dart';
part 'grass.dart';
part 'ladder.dart';
part 'table.dart';
part 'white_wall.dart';
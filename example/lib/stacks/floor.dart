part of voxels;

class Floor extends SpriteStack {
  Floor(Vector2 position)
      : super(
          type: 1,
          dimensions: Vector3(8, 8, 1),
          position: position,
        );
}

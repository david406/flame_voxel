part of voxels;

class Ladder extends SpriteStack {
  Ladder(Vector2 position)
      : super(
          type: 2,
          dimensions: Vector3(8, 8, 8),
          position: position,
        );
}

part of voxels;

class WhiteWall extends SpriteStack {
  WhiteWall(Vector2 position)
      : super(
          type: 4,
          dimensions: Vector3(8, 8, 8),
          position: position,
        );
}

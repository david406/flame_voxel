part of voxels;

class Table extends SpriteStack {
  Table(Vector2 position)
      : super(
          type: 3,
          dimensions: Vector3(8, 8, 3),
          position: position,
        );
}

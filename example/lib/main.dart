import 'dart:math';

import 'package:flame/game.dart';
import 'package:flame/gestures.dart';
import 'package:flame/position.dart';
import 'package:flame/text_config.dart';
import 'package:flame_voxel/flame_voxel.dart';
import 'package:flame_voxel_example/stacks/stacks.dart' as Stacks;
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

void main() => runApp(VoxelGame().widget);

class VoxelGame extends BaseGame with TapDetector, ScaleDetector {
  SpriteWorld _voxelWorld;

  TapDownDetails _tapDownDetails;

  final map = <List<int>>[
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 2, 2, 2, 2, 2, 2, 0, 0, 0],
    [0, 2, 1, 1, 1, 1, 2, 0, 0, 0],
    [0, 2, 1, 1, 1, 1, 2, 2, 2, 0],
    [0, 2, 1, 1, 1, 1, 1, 1, 2, 0],
    [0, 2, 1, 2, 2, 1, 1, 2, 2, 0],
    [0, 2, 1, 2, 2, 1, 1, 2, 0, 0],
    [0, 2, 1, 1, 1, 1, 1, 2, 0, 0],
    [0, 2, 1, 1, 1, 1, 1, 2, 2, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  ];

  Future<void> init() async {
    final voxels = map.fold<List<SpriteStack>>([], (voxels, row) {
      var y = map.indexOf(row).toDouble();
      var x = 0.0;
      return voxels
        ..addAll(
          row.map((type) {
            final pos = Vector2(x++, y);
            switch (type) {
              case 4:
                return Stacks.Table(pos);
              case 3:
                return Stacks.Ladder(pos);
              case 2:
                return Stacks.WhiteWall(pos);
              case 1:
                return Stacks.Floor(pos);
              case 0:
              default:
                return Stacks.Grass(pos);
            }
          }),
        );
    });
    _voxelWorld = await SpriteWorld.withAsset(
      'tileset.png',
      spriteStacks: voxels,
      camera: Camera2D(
        Offset.zero & size,
        zoom: 4,
      ),
      billboardSprites: [
        BillboardSprite(
          size: Vector2(8, 8),
          offset: Vector2(16, 24),
          position: Vector2(2, 0),
        ),
      ],
      quality: SpriteStackQuality.HIGH
    );
  }

  @override
  void resize(Size size) {
    super.resize(size);
    _voxelWorld ?? init();
  }

  @override
  void render(Canvas canvas) {
    if (_voxelWorld == null) return;
    _voxelWorld.render(canvas);

    TextConfig(color: Colors.green, fontSize: 15).render(
      canvas,
      '${fps().toStringAsFixed(2)} FPS',
      Position(0, _voxelWorld.camera.bounds.height - 18),
    );
  }

  @override
  void update(double delta) {
    if (_voxelWorld == null) return;
    _voxelWorld.update(delta);

    // TODO: Implemented proper following
    // _voxelWorld.camera.position =
    //     _voxelWorld.voxels[2].position * (8 * _voxelWorld.camera.zoom);

    if (_tapDownDetails != null) {
      if (_tapDownDetails.localPosition.dx < size.width / 2) {
        _voxelWorld.camera.rotation -= (pi / 180);
      } else {
        _voxelWorld.camera.rotation += (pi / 180);
      }
    }
  }

  @override
  bool recordFps() => true;

  @override
  void onTapDown(TapDownDetails details) => _tapDownDetails = details;

  @override
  void onTapUp(TapUpDetails details) => _tapDownDetails = null;

  @override
  void onTapCancel() => _tapDownDetails = null;

  double _baseScaleFactor;

  @override
  void onScaleStart(_) => _baseScaleFactor = _voxelWorld.camera.zoom;

  @override
  void onScaleUpdate(ScaleUpdateDetails details) {
    _voxelWorld.camera.zoom = _baseScaleFactor * details.scale;
  }
}

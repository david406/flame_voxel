# flame_voxel_example

Simple demonstration of voxels being rendered using the sprite stacking technique.

## Prerequisites
To be able to run the example you first have to slice all the voxels into a single tileset image, thankfully `flame_voxel` has a command for that:
```sh
dart ../bin/slicer.dart --dir ./assets/voxels/ --img ./assets/images/tileset.png
```
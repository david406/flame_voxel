import 'dart:io';
import 'dart:typed_data';
import 'package:args/args.dart';
import 'package:image/image.dart';
import 'package:path/path.dart';

class Voxel {
  final int x, y, z, colorIndex;

  const Voxel(this.x, this.y, this.z, this.colorIndex);

  @override
  String toString() => '{ x: $x, y: $y, z: $z, colorIndex: $colorIndex }';
}

class Color {
  final int r, g, b, a;

  const Color(this.r, this.g, this.b, this.a);

  int pack() {
    var color = 0;
    color |= (r & 0xFF) << 24;
    color |= (g & 0xFF) << 16;
    color |= (b & 0xFF) << 8;
    color |= (a & 0xFF);
    return color;
  }

  @override
  String toString() => '{ r: $r, g: $g, b: $b, a: $a }';

  factory Color.unpack(int rgba) {
    final r = rgba >> 24 & 0xFF;
    final g = rgba >> 16 & 0xFF;
    final b = rgba >> 8 & 0xFF;
    final a = rgba & 0xFF;
    return Color(r, g, b, a);
  }
}

final defaultPal = [
  0x00000000,
  0xffffffff,
  0xffccffff,
  0xff99ffff,
  0xff66ffff,
  0xff33ffff,
  0xff00ffff,
  0xffffccff,
  0xffccccff,
  0xff99ccff,
  0xff66ccff,
  0xff33ccff,
  0xff00ccff,
  0xffff99ff,
  0xffcc99ff,
  0xff9999ff,
  0xff6699ff,
  0xff3399ff,
  0xff0099ff,
  0xffff66ff,
  0xffcc66ff,
  0xff9966ff,
  0xff6666ff,
  0xff3366ff,
  0xff0066ff,
  0xffff33ff,
  0xffcc33ff,
  0xff9933ff,
  0xff6633ff,
  0xff3333ff,
  0xff0033ff,
  0xffff00ff,
  0xffcc00ff,
  0xff9900ff,
  0xff6600ff,
  0xff3300ff,
  0xff0000ff,
  0xffffffcc,
  0xffccffcc,
  0xff99ffcc,
  0xff66ffcc,
  0xff33ffcc,
  0xff00ffcc,
  0xffffcccc,
  0xffcccccc,
  0xff99cccc,
  0xff66cccc,
  0xff33cccc,
  0xff00cccc,
  0xffff99cc,
  0xffcc99cc,
  0xff9999cc,
  0xff6699cc,
  0xff3399cc,
  0xff0099cc,
  0xffff66cc,
  0xffcc66cc,
  0xff9966cc,
  0xff6666cc,
  0xff3366cc,
  0xff0066cc,
  0xffff33cc,
  0xffcc33cc,
  0xff9933cc,
  0xff6633cc,
  0xff3333cc,
  0xff0033cc,
  0xffff00cc,
  0xffcc00cc,
  0xff9900cc,
  0xff6600cc,
  0xff3300cc,
  0xff0000cc,
  0xffffff99,
  0xffccff99,
  0xff99ff99,
  0xff66ff99,
  0xff33ff99,
  0xff00ff99,
  0xffffcc99,
  0xffcccc99,
  0xff99cc99,
  0xff66cc99,
  0xff33cc99,
  0xff00cc99,
  0xffff9999,
  0xffcc9999,
  0xff999999,
  0xff669999,
  0xff339999,
  0xff009999,
  0xffff6699,
  0xffcc6699,
  0xff996699,
  0xff666699,
  0xff336699,
  0xff006699,
  0xffff3399,
  0xffcc3399,
  0xff993399,
  0xff663399,
  0xff333399,
  0xff003399,
  0xffff0099,
  0xffcc0099,
  0xff990099,
  0xff660099,
  0xff330099,
  0xff000099,
  0xffffff66,
  0xffccff66,
  0xff99ff66,
  0xff66ff66,
  0xff33ff66,
  0xff00ff66,
  0xffffcc66,
  0xffcccc66,
  0xff99cc66,
  0xff66cc66,
  0xff33cc66,
  0xff00cc66,
  0xffff9966,
  0xffcc9966,
  0xff999966,
  0xff669966,
  0xff339966,
  0xff009966,
  0xffff6666,
  0xffcc6666,
  0xff996666,
  0xff666666,
  0xff336666,
  0xff006666,
  0xffff3366,
  0xffcc3366,
  0xff993366,
  0xff663366,
  0xff333366,
  0xff003366,
  0xffff0066,
  0xffcc0066,
  0xff990066,
  0xff660066,
  0xff330066,
  0xff000066,
  0xffffff33,
  0xffccff33,
  0xff99ff33,
  0xff66ff33,
  0xff33ff33,
  0xff00ff33,
  0xffffcc33,
  0xffcccc33,
  0xff99cc33,
  0xff66cc33,
  0xff33cc33,
  0xff00cc33,
  0xffff9933,
  0xffcc9933,
  0xff999933,
  0xff669933,
  0xff339933,
  0xff009933,
  0xffff6633,
  0xffcc6633,
  0xff996633,
  0xff666633,
  0xff336633,
  0xff006633,
  0xffff3333,
  0xffcc3333,
  0xff993333,
  0xff663333,
  0xff333333,
  0xff003333,
  0xffff0033,
  0xffcc0033,
  0xff990033,
  0xff660033,
  0xff330033,
  0xff000033,
  0xffffff00,
  0xffccff00,
  0xff99ff00,
  0xff66ff00,
  0xff33ff00,
  0xff00ff00,
  0xffffcc00,
  0xffcccc00,
  0xff99cc00,
  0xff66cc00,
  0xff33cc00,
  0xff00cc00,
  0xffff9900,
  0xffcc9900,
  0xff999900,
  0xff669900,
  0xff339900,
  0xff009900,
  0xffff6600,
  0xffcc6600,
  0xff996600,
  0xff666600,
  0xff336600,
  0xff006600,
  0xffff3300,
  0xffcc3300,
  0xff993300,
  0xff663300,
  0xff333300,
  0xff003300,
  0xffff0000,
  0xffcc0000,
  0xff990000,
  0xff660000,
  0xff330000,
  0xff0000ee,
  0xff0000dd,
  0xff0000bb,
  0xff0000aa,
  0xff000088,
  0xff000077,
  0xff000055,
  0xff000044,
  0xff000022,
  0xff000011,
  0xff00ee00,
  0xff00dd00,
  0xff00bb00,
  0xff00aa00,
  0xff008800,
  0xff007700,
  0xff005500,
  0xff004400,
  0xff002200,
  0xff001100,
  0xffee0000,
  0xffdd0000,
  0xffbb0000,
  0xffaa0000,
  0xff880000,
  0xff770000,
  0xff550000,
  0xff440000,
  0xff220000,
  0xff110000,
  0xffeeeeee,
  0xffdddddd,
  0xffbbbbbb,
  0xffaaaaaa,
  0xff888888,
  0xff777777,
  0xff555555,
  0xff444444,
  0xff222222,
  0xff111111
].map((c) => Color.unpack(c)).toList();

int voxXDim;
int voxYDim;
int voxZDim;

void die(String msg) {
  stderr.writeln('[fatal] $msg');
  exit(1);
}

bool equals(int charCode, String char) {
  try {
    return String.fromCharCode(charCode) == char;
  } catch (_) {
    return false;
  }
}

Future<List<Image>> traverse(
  String path, {
  List<Image> images,
}) async {
  images ??= [];
  if (FileSystemEntity.typeSync(path) == FileSystemEntityType.directory) {
    for (var fileEntity in Directory.fromUri(Uri.parse(path)).listSync()) {
      await traverse(fileEntity.path, images: images);
    }
    return images;
  }
  images.add(await slice(path));
  return images;
}

/// Slices the voxel into layers and writes them to an image.
///
/// Code has been adapted from https://github.com/jemisa/vox2png.
/// For reference of the voxel file see: https://github.com/ephtracy/voxel-model/blob/master/MagicaVoxel-file-format-vox.txt
Future<Image> slice(String path) async {
  final voxHandle = await File(path).open(mode: FileMode.read);
  if (basename(voxHandle.path).split('.')[1] != 'vox') {
    die('$path does not have a .vox extension');
  }

  final voxBuffer = ByteData.view(
    voxHandle.readSync(voxHandle.lengthSync()).buffer,
  );

  if (voxBuffer.buffer.lengthInBytes < 8 ||
      !equals(voxBuffer.getInt8(0), 'V') ||
      !equals(voxBuffer.getInt8(1), 'O') ||
      !equals(voxBuffer.getInt8(2), 'X') ||
      !equals(voxBuffer.getInt8(3), ' ')) {
    die('$path is either corrupted, or not a .vox file');
  }
  final voxelVersion = voxBuffer.getUint8(4);

  // Read the SIZE chunk to get the dimensions of the voxel object.
  // int voxXDim, voxYDim, voxZDim;
  var sizeChunkFound = false;
  for (var i = 0; i < voxBuffer.buffer.lengthInBytes - 4; ++i) {
    if (equals(voxBuffer.getInt8(i + 0), 'S') &&
        equals(voxBuffer.getInt8(i + 1), 'I') &&
        equals(voxBuffer.getInt8(i + 2), 'Z') &&
        equals(voxBuffer.getInt8(i + 3), 'E')) {
      voxXDim ??= voxBuffer.getInt8(i + 12 + 4 * 0);
      if (voxXDim != voxBuffer.getInt8(i + 12 + 4 * 0)) {
        print('$path x dimension is not equal to $voxXDim');
      }
      voxYDim ??= voxBuffer.getInt8(i + 12 + 4 * 1);
      if (voxYDim != voxBuffer.getInt8(i + 12 + 4 * 1)) {
        print('$path y dimension is not equal to $voxYDim');
      }
      voxZDim ??= voxBuffer.getInt8(i + 12 + 4 * 2);
      if (voxZDim != voxBuffer.getInt8(i + 12 + 4 * 2)) {
        print('$path z dimension is not equal to $voxZDim');
      }
      sizeChunkFound = true;
      break;
    }
  }
  if (!sizeChunkFound) {
    die('The .vox file is missing a SIZE chunk');
  }

  // Read the XYZI chunk to get the voxel data.
  int voxCount;
  var voxVoxels = <Voxel>[];
  var voxelsFound = false;
  for (var i = 0; i < voxBuffer.buffer.lengthInBytes - 4; ++i) {
    if (equals(voxBuffer.getInt8(i + 0), 'X') &&
        equals(voxBuffer.getInt8(i + 1), 'Y') &&
        equals(voxBuffer.getInt8(i + 2), 'Z') &&
        equals(voxBuffer.getInt8(i + 3), 'I')) {
      voxCount = voxBuffer.getInt32(i + 12, Endian.little);
      for (var j = 0; j < voxCount; j++) {
        voxVoxels.add(Voxel(
          voxBuffer.getInt8(i + 12 + 4 + 0 + (j * 4)),
          voxBuffer.getInt8(i + 12 + 4 + 1 + (j * 4)),
          voxBuffer.getInt8(i + 12 + 4 + 2 + (j * 4)),
          voxBuffer.getUint8(i + 12 + 4 + 3 + (j * 4)),
        ));
      }
      voxelsFound = true;
      break;
    }
  }
  if (!voxelsFound) {
    die('The .vox file is missing a XYZI chunk');
  }

  var voxPal = <Color>[];
  for (var i = 0; i < voxBuffer.buffer.lengthInBytes - 4; i++) {
    if (equals(voxBuffer.getInt8(i + 0), 'R') &&
        equals(voxBuffer.getInt8(i + 1), 'G') &&
        equals(voxBuffer.getInt8(i + 2), 'B') &&
        equals(voxBuffer.getInt8(i + 3), 'A')) {
      for (var j = 0; j <= 254; j++) {
        voxPal.add(Color.unpack(voxBuffer.getUint32(i + 12 + j * 4)));
      }
      break;
    }
  }
  if (voxPal.length == 0) {
    voxPal = defaultPal;
  }

  print(
    'Loaded $path into memory:\n'
    ' version:  $voxelVersion\n'
    ' size:     [$voxXDim, $voxYDim, $voxZDim]\n'
    ' voxels:   $voxCount\n'
    ' palette:  ${voxPal == defaultPal ? 'default' : 'custom'}',
  );

  // Create the Png image data and write our voxels to it.
  var pngWidth = voxXDim * voxZDim;
  var pngHeight = voxYDim;
  var xCells = voxZDim;
  var yCells = 1;

  final pngData = List<int>(pngWidth * pngHeight * 4);
  voxVoxels.forEach((currentVoxel) {
    final currentColor = voxPal[currentVoxel.colorIndex - 1];

    int dataX = currentVoxel.x;
    int dataY = currentVoxel.y;
    dataX += currentVoxel.z * 8 % (xCells * voxXDim);
    dataY += currentVoxel.z ~/ (xCells * voxYDim);
    int dataIndex = (dataX + dataY * pngWidth) * 4;

    pngData[dataIndex] = currentColor.r;
    pngData[dataIndex + 1] = currentColor.g;
    pngData[dataIndex + 2] = currentColor.b;
    pngData[dataIndex + 3] = currentColor.a;
  });

  // Write the image data to the Png
  var image = Image.fromBytes(
    pngWidth,
    pngHeight,
    pngData.map((e) => e ?? 0).toList(),
  );
  print('Converted $path into png data.');
  return image;
}

void main(List<String> arguments) async {
  final parser = ArgParser()
    ..addOption(
      'dir',
      abbr: 'd',
      help: 'The starting directory for voxels',
      valueHelp: 'String',
    )
    ..addOption(
      'img',
      abbr: 'i',
      help: 'The output image for all the found voxels',
      valueHelp: 'String',
    )
    ..addFlag('help', abbr: 'h', help: 'This help text', negatable: false);

  final result = parser.parse(arguments);
  if (result['help']) {
    print('Usage: vox2png [options...]');
    print(' ' + parser.usage.replaceAll('\n', '\n '));
    exit(0);
  }
  if (result['dir'] == null) die('Missing --dir option');
  if (result['img'] == null) die('Missing --img option');

  final dir = result['dir'];
  final img = result['img'];
  final srcImages = await traverse(dir);
  final dstImage = Image(8 * 8, 8 * srcImages.length);

  for (var i = 0; i < srcImages.length; i++) {
    final srcImage = srcImages[i];
    drawImage(dstImage, srcImage, dstY: i * 8);
  }
  File(img).writeAsBytesSync(encodePng(dstImage));
}

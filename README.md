# flame_voxel

Build voxel-like games using the sprite stacking technique and the power of the [Flame](https://flame-engine.org/) engine.

This package is currently work-in-progress, We can't guarantee that the current API implementation will stay the same between versions, until we have reached v1.0.0.

Documentation is not yet written for now please checkout the source code and generated API docs.